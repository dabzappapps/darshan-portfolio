package com.it.dwitiyabhatt.darshanportfolio.fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentInfo;
import android.print.PrintManager;
import android.print.pdf.PrintedPdfDocument;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;
import com.github.barteksc.pdfviewer.scroll.DefaultScrollHandle;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.it.dwitiyabhatt.darshanportfolio.MyPortFolioApp;
import com.it.dwitiyabhatt.darshanportfolio.activities.HomeActivity;
import com.it.dwitiyabhatt.darshanportfolio.baseclasses.BaseFragment;
import com.it.dwitiyabhatt.darshanportfolio.utils.Constants;
import com.it.dwitiyabhatt.darshanportfolio.BuildConfig;
import com.it.dwitiyabhatt.darshanportfolio.R;
import com.shockwave.pdfium.PdfDocument;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.PRINT_SERVICE;
import static com.it.dwitiyabhatt.darshanportfolio.utils.Constants.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE;

public class MyResumeFragment extends BaseFragment implements OnPageChangeListener,OnLoadCompleteListener {


    public static final String SAMPLE_FILE = "android_tutorial.pdf";

    @BindView(R.id.pdfView)
    PDFView pdfView;

    @BindView(R.id.tvNoData)
    TextView tvNoData;

    @BindView(R.id.frProgress)
    FrameLayout frProgress;

    @BindView(R.id.fabAdd)
    FloatingActionButton fabAdd;

    @BindView(R.id.fabShare)
    FloatingActionButton fabShare;

    @BindView(R.id.fabUpload)
    FloatingActionButton fabUpload;

    @BindView(R.id.fabPrint)
    FloatingActionButton fabPrint;

    Integer pageNumber = 0;
    String pdfFileName;
    private boolean isFabOpen = false;
    StorageReference mStorageReference;
    DatabaseReference mDatabaseReference;

    private File file,downloadedFile;

    private Activity mActivity;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflating view layout
        View layout = inflater.inflate(R.layout.fragment_my_resume, container, false);
        ButterKnife.bind(this, layout);
        initToolbar();
        mStorageReference = FirebaseStorage.getInstance().getReference();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        tvNoData.setText(R.string.no_resume_msg);
        checkAndLoadLocalFIle();
        //downloadPDF();
        return layout;
    }

    public void initToolbar() {

        mActivity = MyPortFolioApp.getmInstance().getActivity();
        ((HomeActivity) mActivity).
                setUpToolbar(mActivity.getString(R.string.my_resume_pdf), true,false);



    }

    private void uploadPDF() {
        //for greater than lolipop versions we need the permissions asked on runtime
        //so if the permission is not available user will go to the screen to allow storage permission

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(mActivity,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                    Uri.parse("package:" + mActivity.getPackageName()));
            startActivity(intent);
            return;
        }

        //creating an intent for file chooser
        Intent intent = new Intent();
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select file"), Constants.UPLOAD_PDF);
    }

    private void displayFromAsset(String assetFileName) {
        pdfFileName = assetFileName;

        pdfView.fromAsset(assetFileName)
                .defaultPage(pageNumber)
                .enableSwipe(true)
                .swipeHorizontal(false)
                .onPageChange(this)
                .enableAnnotationRendering(true)
                .onLoad(this)
                .scrollHandle(new DefaultScrollHandle(mActivity))
                .load();
    }
    private void displayFromFile(File file) {

        pdfView.fromFile(file)
                .defaultPage(pageNumber)
                .enableSwipe(true)
                .swipeHorizontal(false)
                .onPageChange(this)
                .enableAnnotationRendering(true)
                .onLoad(this)
                .scrollHandle(new DefaultScrollHandle(mActivity))
                .load();
    }

    private void checkAndLoadLocalFIle(){
        File mainDir = new File(Environment.getExternalStorageDirectory(),
                Constants.MAIN_DIRECTORY + Constants.RESUME_DIRECTORY );
        if(mainDir.exists()){
            file  =  new File(mainDir.getAbsolutePath(), "sample_resume" + ".pdf");

            if(file.exists()) displayFromFile(file);
            else downloadPDF();
        }else{
            downloadPDF();
        }

    }

    private void shareFile(){
        try {
            Intent intentShareFile = new Intent(Intent.ACTION_SEND);
            //File fileWithinMyDir =  file;

            if(file !=null && file.exists()) {
                intentShareFile.setType("application/pdf");
                intentShareFile.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                Uri uri = FileProvider.getUriForFile(mActivity,
                        BuildConfig.APPLICATION_ID + ".provider",file);
                intentShareFile.putExtra(Intent.EXTRA_STREAM, uri);

                intentShareFile.putExtra(Intent.EXTRA_SUBJECT,
                        "Sharing File...");
                intentShareFile.putExtra(Intent.EXTRA_TEXT, "Sharing File via my portfolio app.");

                startActivity(Intent.createChooser(intentShareFile, "Share File"));
            }else{
                Toast.makeText(mActivity, "File not found", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(mActivity, "File cannot be stored because it is not stored locally.", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //when the user choses the file
        if (requestCode == Constants.UPLOAD_PDF && resultCode == RESULT_OK && data != null && data.getData() != null) {
            //if a file is selected
            if (data.getData() != null) {
                //uploading the file
                showHideProgress(true,frProgress);
                uploadFile(data.getData());
            }else{
                Toast.makeText(mActivity, "No file chosen", Toast.LENGTH_SHORT).show();
            }
        }
    }


    //this method is uploading the file
    //the code is same as the previous tutorial
    //so we are not explaining it
    private void uploadFile(Uri data) {

        StorageReference sRef = mStorageReference.child(Constants.STORAGE_PATH_UPLOADS +
                ((HomeActivity)getActivity()).getLoggedInUserId()+"_resume_"+".pdf");
        sRef.putFile(data)
                .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @SuppressWarnings("VisibleForTests")
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                        showHideProgress(false,frProgress);
                        Toast.makeText(mActivity, "File uploaded successfully", Toast.LENGTH_SHORT).show();
                       // Upload upload = new Upload(editTextFilename.getText().toString(), taskSnapshot.getDownloadUrl().toString());
                        //mDatabaseReference.child(mDatabaseReference.push().getKey()).setValue(upload);

                        downloadPDF();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        Toast.makeText(mActivity, exception.getMessage(), Toast.LENGTH_LONG).show();
                    }
                })
                .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                    @SuppressWarnings("VisibleForTests")
                    @Override
                    public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                        double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                        //textViewStatus.setText((int) progress + "% Uploading...");
                    }
                });

    }

    private void downloadPDF(){
        try {

            showHideProgress(true,frProgress);

            StorageReference sRef = mStorageReference.child(Constants.STORAGE_PATH_UPLOADS
                    +"sample_resume"+".pdf");;
            final File localFile = File.createTempFile("resume", "pdf");

            sRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {
                    // Local temp file has been created
                    tvNoData.setVisibility(View.GONE);
                    file = localFile;
                    displayFromFile(localFile);
                    checkPermission(mActivity);



                    showHideProgress(false,frProgress);

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Toast.makeText(mActivity, "Failure", Toast.LENGTH_SHORT).show();
                    showHideProgress(false,frProgress);
                    tvNoData.setVisibility(View.VISIBLE);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onPageChanged(int page, int pageCount) {
        pageNumber = page;
        //setTitle(String.format("%s %s / %s", pdfFileName, page + 1, pageCount));
    }


    @Override
    public void loadComplete(int nbPages) {
        PdfDocument.Meta meta = pdfView.getDocumentMeta();
        printBookmarksTree(pdfView.getTableOfContents(), "-");

    }

    public void printBookmarksTree(List<PdfDocument.Bookmark> tree, String sep) {
        for (PdfDocument.Bookmark b : tree) {
            if (b.hasChildren()) {
                printBookmarksTree(b.getChildren(), sep + "-");
            }
        }
    }

    private void showFABMenu(){
        isFabOpen=true;

        fabPrint.animate().translationY(-getResources().getDimension(R.dimen._45sdp));
        fabShare.animate().translationY(-getResources().getDimension(R.dimen._90sdp));
        fabUpload.animate().translationY(-getResources().getDimension(R.dimen._135sdp));
    }

    private void closeFABMenu(){
        isFabOpen=false;
        fabPrint.animate().translationY(0);
        fabShare.animate().translationY(0);
        fabUpload.animate().translationY(0);
    }

    @OnClick({R.id.fabAdd,R.id.fabShare, R.id.fabPrint,R.id.fabUpload})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.fabAdd:
                if(isFabOpen)closeFABMenu();
                else showFABMenu();
                break;
            case R.id.fabShare:
                shareFile();
                break;
            case R.id.fabPrint:
                //downloadPDF();
                printPDF(pdfView);

                break;
            case R.id.fabUpload:
                uploadPDF();
                break;

        }

    }
/*
    private void shareFile(){
        try {
            Intent intentShareFile = new Intent(Intent.ACTION_SEND);
            //File fileWithinMyDir =  file;

            if(file !=null && file.exists()) {
                intentShareFile.setType("application/pdf");
                intentShareFile.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://"+file.getAbsolutePath()));

                intentShareFile.putExtra(Intent.EXTRA_SUBJECT,
                        "Sharing File...");
                intentShareFile.putExtra(Intent.EXTRA_TEXT, "Sharing File via my portfolio app.");

                startActivity(Intent.createChooser(intentShareFile, "Share File"));
            }else{
                Toast.makeText(mActivity, "File not found", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    private boolean checkPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(mActivity,
                    Manifest.permission.READ_EXTERNAL_STORAGE) !=
                    PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    Toast.makeText(context, "Please allow read storage permission from settings to save resume on phone", Toast.LENGTH_SHORT).show();
                } else {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                saveFileToMyDirectory();
                return true;
            }
        } else {
            return true;
        }
    }

    private void saveFileToMyDirectory(){
        try {
            File mainDir = new File(Environment.getExternalStorageDirectory(),
                    Constants.MAIN_DIRECTORY + Constants.RESUME_DIRECTORY);
            if (! mainDir.exists()){
                mainDir.mkdirs();
            }
            downloadedFile  =  new File(mainDir.getAbsolutePath(), "sample_resume" + ".pdf");
            FileInputStream inStream = new FileInputStream(file.getAbsolutePath());
            FileOutputStream outStream = new FileOutputStream(downloadedFile);
            FileChannel inChannel = inStream.getChannel();
            FileChannel outChannel = outStream.getChannel();
            inChannel.transferTo(0, inChannel.size(), outChannel);
            inStream.close();
            outStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        }





    public void printPDF(View view) {
        PrintManager printManager = (PrintManager) mActivity.getSystemService(PRINT_SERVICE);
        printManager.print("darshan_resume", new ViewPrintAdapter(mActivity,
                view), null);
    }

    public class ViewPrintAdapter extends PrintDocumentAdapter {

        private PrintedPdfDocument mDocument;
        private Context mContext;
        private View mView;

        public ViewPrintAdapter(Context context, View view) {
            mContext = context;
            mView = view;
        }

        @Override
        public void onLayout(PrintAttributes oldAttributes, PrintAttributes newAttributes,
                             CancellationSignal cancellationSignal,
                             LayoutResultCallback callback, Bundle extras) {

            mDocument = new PrintedPdfDocument(mContext, newAttributes);

            if (cancellationSignal.isCanceled()) {
                callback.onLayoutCancelled();
                return;
            }

            PrintDocumentInfo.Builder builder = new PrintDocumentInfo
                    .Builder("print_output.pdf")
                    .setContentType(PrintDocumentInfo.CONTENT_TYPE_DOCUMENT)
                    .setPageCount(1);

            PrintDocumentInfo info = builder.build();
            callback.onLayoutFinished(info, true);
        }



        @Override
        public void onWrite(PageRange[] pages, ParcelFileDescriptor destination,
                            CancellationSignal cancellationSignal,
                            WriteResultCallback callback) {

            // Start the page

            android.graphics.pdf.PdfDocument.Page page = mDocument.startPage(0);
            // Create a bitmap and put it a canvas for the view to draw to. Make it the size of the view
            Bitmap bitmap = Bitmap.createBitmap(mView.getWidth(), mView.getHeight(),
                    Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            mView.draw(canvas);
            // create a Rect with the view's dimensions.
            Rect src = new Rect(0, 0, mView.getWidth(), mView.getHeight());
            // get the page canvas and measure it.
            Canvas pageCanvas = page.getCanvas();
            float pageWidth = pageCanvas.getWidth();
            float pageHeight = pageCanvas.getHeight();
            // how can we fit the Rect src onto this page while maintaining aspect ratio?
            float scale = Math.min(pageWidth/src.width(), pageHeight/src.height());
            float left = pageWidth / 2 - src.width() * scale / 2;
            float top = pageHeight / 2 - src.height() * scale / 2;
            float right = pageWidth / 2 + src.width() * scale / 2;
            float bottom = pageHeight / 2 + src.height() * scale / 2;
            RectF dst = new RectF(left, top, right, bottom);

            pageCanvas.drawBitmap(bitmap, src, dst, null);
            mDocument.finishPage(page);

            try {
                mDocument.writeTo(new FileOutputStream(
                        destination.getFileDescriptor()));
            } catch (IOException e) {
                callback.onWriteFailed(e.toString());
                return;
            } finally {
                mDocument.close();
                mDocument = null;
            }
            callback.onWriteFinished(new PageRange[]{new PageRange(0, 0)});
        }
    }


    private File getPDFFile(){
        File f = new File(mActivity.getCacheDir()+"/darshan_resume.pdf");
        if (!f.exists())
            try {
            InputStream is = mActivity.getAssets().open("darshan_resume.pdf");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            FileOutputStream fos = new FileOutputStream(f);
            fos.write(buffer);
            fos.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return f;

    }


}




