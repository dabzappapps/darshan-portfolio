package com.it.dwitiyabhatt.darshanportfolio.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.it.dwitiyabhatt.darshanportfolio.baseclasses.BaseRecyclerAdapter;
import com.it.dwitiyabhatt.darshanportfolio.models.SingleSelectionModel;
import com.it.dwitiyabhatt.darshanportfolio.R;

import java.util.ArrayList;

/**
 * Created by user15 on 9/11/17.
 */

public class AchivementsAdapter
        extends BaseRecyclerAdapter<AchivementsAdapter.DataViewHolder, SingleSelectionModel> {


    private ArrayList<SingleSelectionModel> categoryArrayList;
    private Context context;
    private boolean singleSelection=true;
    private int lastSelectedPosition = 0;

    public AchivementsAdapter(Context context, ArrayList<SingleSelectionModel> categoryArrayList) {
        super(categoryArrayList);
        this.categoryArrayList = categoryArrayList;
        this.context = context;

    }

    public AchivementsAdapter(ArrayList<SingleSelectionModel> categoryArrayList, Context context, boolean singleSelection) {
        super(categoryArrayList);
        this.categoryArrayList = categoryArrayList;
        this.context = context;
        this.singleSelection = singleSelection;
    }

    public ArrayList<SingleSelectionModel> getNavItemArrayList() {
        return categoryArrayList;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DataViewHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_achivements,
                parent, false));
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, final int position) {


    }

    class DataViewHolder extends BaseRecyclerAdapter.ViewHolder {


        public DataViewHolder(View itemView) {
            super(itemView);
            clickableViews(itemView);

        }
    }

    public int getLastSelectedPosition() {
        return lastSelectedPosition;
    }

    public void setLastSelectedPosition(int lastSelectedPosition) {
        this.lastSelectedPosition = lastSelectedPosition;
    }
}

