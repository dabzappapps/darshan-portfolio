package com.it.dwitiyabhatt.darshanportfolio.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.it.dwitiyabhatt.darshanportfolio.MyPortFolioApp;
import com.it.dwitiyabhatt.darshanportfolio.activities.HomeActivity;
import com.it.dwitiyabhatt.darshanportfolio.utils.TouchImageView;
import com.it.dwitiyabhatt.darshanportfolio.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AchivementsDetailFragment extends Fragment {

    @BindView(R.id.ivCertificate)
    TouchImageView ivCertificate;

    private Activity mActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflating view layout
        View layout = inflater.inflate(R.layout.fragment_achivements_details, container, false);
        ButterKnife.bind(this, layout);
        initToolbar();
        ivCertificate.setImageResource(R.drawable.ssc);
        return layout;
    }

    public void initToolbar() {
        mActivity = MyPortFolioApp.getmInstance().getActivity();

        {
            ((HomeActivity) mActivity).
                    setUpToolbar(mActivity.getString(R.string.certificate), true, true);


        }


    }
}
