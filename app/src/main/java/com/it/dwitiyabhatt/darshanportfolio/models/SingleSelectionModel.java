package com.it.dwitiyabhatt.darshanportfolio.models;

public class SingleSelectionModel {

    private String id,title;
    private boolean isSelected = false;


    public SingleSelectionModel() {
    }

    public SingleSelectionModel(String title) {
        this.title = title;
    }

    public SingleSelectionModel(String id, String title) {
        this.id = id;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
