package com.it.dwitiyabhatt.darshanportfolio.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.it.dwitiyabhatt.darshanportfolio.MyPortFolioApp;
import com.it.dwitiyabhatt.darshanportfolio.activities.HomeActivity;
import com.it.dwitiyabhatt.darshanportfolio.adapters.MyCompaniesAdapter;
import com.it.dwitiyabhatt.darshanportfolio.baseclasses.BaseFragment;
import com.it.dwitiyabhatt.darshanportfolio.baseclasses.BaseRecyclerAdapter;
import com.it.dwitiyabhatt.darshanportfolio.models.Company;
import com.it.dwitiyabhatt.darshanportfolio.utils.Constants;
import com.it.dwitiyabhatt.darshanportfolio.utils.Util;
import com.it.dwitiyabhatt.darshanportfolio.R;

import java.util.ArrayList;
import java.util.Iterator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyCompaniesFragment extends BaseFragment {

    @BindView(R.id.recyclerView)
    RecyclerView rvCourses;

    @BindView(R.id.fabAdd)
    FloatingActionButton fabAdd;

    @BindView(R.id.frProgress)
    FrameLayout frProgress;

    @BindView(R.id.tvNoData)
    TextView tvNoData;

    private ArrayList<Company> companyArrayList = new ArrayList<>();
    private MyCompaniesAdapter myCarrearAdapter;
    private DatabaseReference mDatabase;

    private Activity mActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflating view layout
        View layout = inflater.inflate(R.layout.fragment_my_projects, container, false);
        ButterKnife.bind(this, layout);
        initToolbar();

        showHideProgress(true,frProgress);

        mDatabase = FirebaseDatabase.getInstance().
                getReference().
                child(Constants.USERS)
                .child(((HomeActivity)getActivity()).getLoggedInUserId()).
                        child(Constants.COMPANIES);

        setupAdapter();

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                companyArrayList.clear();
                myCarrearAdapter.notifyDataSetChanged();

                Iterator<DataSnapshot> iter = dataSnapshot.getChildren().iterator();

                if(iter.hasNext()) {
                    tvNoData.setVisibility(View.GONE);
                    while (iter.hasNext()) {
                        Company company = iter.next().getValue(Company.class);
                        companyArrayList.add(company);
                    }
                    myCarrearAdapter.notifyDataSetChanged();
                }else{
                    tvNoData.setVisibility(View.VISIBLE);
                }

                showHideProgress(false,frProgress);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });




        return layout;
    }


    private void setupAdapter() {
        rvCourses.setLayoutManager(new LinearLayoutManager(mActivity));
        myCarrearAdapter = new MyCompaniesAdapter(mActivity, companyArrayList);
        rvCourses.setAdapter(myCarrearAdapter.setRecycleOnItemClickListener(mRecycleOnItemClickListener));
    }

    private BaseRecyclerAdapter.RecycleOnItemClickListener mRecycleOnItemClickListener = new BaseRecyclerAdapter.RecycleOnItemClickListener() {
        @Override
        public void onItemClick(View view, final int position)
        {
            switch (view.getId()){
                case R.id.tvDetails:
                    myCarrearAdapter.setLastSelectedItem(position);
                    myCarrearAdapter.notifyDataSetChanged();
                    break;
                case R.id.tvEdit:
                    /*AddProjectsFragment projectsFragment = new AddProjectsFragment();
                    Bundle b =new Bundle();
                    b.putString("id",projectArrayList.get(position).getId());
                    projectsFragment.setArguments(b);
                    Util.addNextFragment(mActivity,projectsFragment,
                            MyProjectsFragment.this,false);*/
                    break;

            }
        }
    };

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden)initToolbar();
    }

    public void initToolbar() {
        mActivity = MyPortFolioApp.getmInstance().getActivity();

            ((HomeActivity) mActivity).
                    setUpToolbar(getString(R.string.nav_my_companies), true,false);



    }

    @OnClick({R.id.fabAdd})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fabAdd:
                Util.addNextFragment(mActivity,new AddCompaniesFragment(), MyCompaniesFragment.this,false);
                break;
        }
    }





}
