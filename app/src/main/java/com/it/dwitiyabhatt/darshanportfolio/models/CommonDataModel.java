package com.it.dwitiyabhatt.darshanportfolio.models;

public class CommonDataModel {

    private String id,title,descr;

    public CommonDataModel(String id, String title, String descr) {
        this.id = id;
        this.title = title;
        this.descr = descr;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }
}
