package com.it.dwitiyabhatt.darshanportfolio.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.it.dwitiyabhatt.darshanportfolio.MyPortFolioApp;
import com.it.dwitiyabhatt.darshanportfolio.activities.SplashActivity;
import com.it.dwitiyabhatt.darshanportfolio.adapters.LanguageSelectionAdapter;
import com.it.dwitiyabhatt.darshanportfolio.adapters.ThemeSelectionAdapter;
import com.it.dwitiyabhatt.darshanportfolio.baseclasses.BaseRecyclerAdapter;
import com.it.dwitiyabhatt.darshanportfolio.models.SingleSelectionModel;
import com.it.dwitiyabhatt.darshanportfolio.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SettingsFragment extends Fragment {



    @BindView(R.id.rbtnGrpLanguage)
    RadioGroup rbtnGrpLanguage;

    private ArrayList<SingleSelectionModel> languageArrayList;
    private ArrayList<SingleSelectionModel> themeArrayList;
    private LanguageSelectionAdapter languageSelectionAdapter;
    private ThemeSelectionAdapter themeSelectionAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflating view layout
        View layout = inflater.inflate(R.layout.fragment_settings, container, false);
        ButterKnife.bind(this, layout);

        initialize();

        return layout;
    }

    private void initialize() {

        String languageToLoad  = MyPortFolioApp.getmInstance().getSharedPreferences().
                getString(getString((R.string.preference_param_user_language)), "en");

        if(languageToLoad.equals("fr" +
                "")){
            rbtnGrpLanguage.check(R.id.rbtnFrench);
        }
        else if(languageToLoad.equals("pj")){
            rbtnGrpLanguage.check(R.id.rbtnPunjabi);
        }else{
            rbtnGrpLanguage.check(R.id.rbtnEnglish);
        }


        rbtnGrpLanguage.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {

                String lan;
                switch (i){
                    case  R.id.rbtnEnglish:
                        lan ="en";
                        break;
                    case R.id.rbtnFrench:
                        lan = "fr";
                        break;
                    case R.id.rbtnPunjabi:
                        lan = "pj";
                        break;
                        default:
                            lan ="en";
                }


                MyPortFolioApp.getmInstance().savePreferenceDataString
                        (getString(R.string.preference_param_user_language),
                                lan);

                restartApp();
            }
        });
    }


    private void fillLanguageData() {
        languageArrayList.add(new SingleSelectionModel("en","English"));
        languageArrayList.add(new SingleSelectionModel("fr","French"));
        languageArrayList.add(new SingleSelectionModel("pj","Punjabi"));
        languageSelectionAdapter.notifyDataSetChanged();

        String selectedLanguage  = MyPortFolioApp.getmInstance().getSharedPreferences().
                getString(getString((R.string.preference_param_user_language)), "en");
        int count=0;
        for(SingleSelectionModel singleSelectionModel: languageArrayList){
            if(selectedLanguage.equals(singleSelectionModel.getId())){
                singleSelectionModel.setSelected(true);
                languageSelectionAdapter.setLastSelectedPosition(count);
                }
            count++;
        }
        languageSelectionAdapter.notifyDataSetChanged();
    }



    private BaseRecyclerAdapter.RecycleOnItemClickListener languageItemListener =
            new BaseRecyclerAdapter.RecycleOnItemClickListener() {
        @Override
        public void onItemClick(View view, final int position) {

            SingleSelectionModel singleSelectionModel = languageArrayList.get(position);

            switch (view.getId()){
                case R.id.rvParent:

                    break;
            }


        }
    };


    private BaseRecyclerAdapter.RecycleOnItemClickListener themeItemListener =
            new BaseRecyclerAdapter.RecycleOnItemClickListener() {
                @Override
                public void onItemClick(View view, final int position) {

                    SingleSelectionModel singleSelectionModel = themeArrayList.get(position);

                    languageSelectionAdapter.setLastSelectedPosition(position);
                   /* if(singleSelectionModel.isSelected())singleSelectionModel.setSelected(false);
                    else  singleSelectionModel.setSelected(true);*/

                    themeSelectionAdapter.notifyDataSetChanged();
                }
            };

    private void restartApp() {
        Intent intent = new Intent(getActivity(), SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        getActivity().finish();

    }

}
