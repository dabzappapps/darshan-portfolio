package com.it.dwitiyabhatt.darshanportfolio.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.it.dwitiyabhatt.darshanportfolio.baseclasses.BaseRecyclerAdapter;
import com.it.dwitiyabhatt.darshanportfolio.models.Project;
import com.it.dwitiyabhatt.darshanportfolio.R;

import java.util.ArrayList;

import butterknife.BindView;

public class MyProjectsAdapter extends BaseRecyclerAdapter<MyProjectsAdapter.DataViewHolder, Project> {


    private ArrayList<Project> projectArrayList;
    private Context context;
    private int lastSelectedItem = -1;


    public MyProjectsAdapter(Context context, ArrayList<Project> chatArrayList) {
        super(chatArrayList);
        this.projectArrayList = chatArrayList;
        this.context = context;


    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DataViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_projects,
                parent, false));
    }

    @Override
    public void onBindViewHolder(final DataViewHolder holder, final int position) {
        Project project = projectArrayList.get(position);
        /*final boolean isExpanded = position==lastSelectedItem;
        holder.linDescription.setVisibility(isExpanded?View.VISIBLE:View.GONE);
        holder.itemView.setActivated(isExpanded);

        if (isExpanded)
            previousExpandedPosition = position;
*/
        if(lastSelectedItem == position){
            holder.linDescription.setVisibility(View.VISIBLE);
            holder.itemView.setActivated(true);
        }else{
            holder.linDescription.setVisibility(View.GONE);
            holder.itemView.setActivated(true);
        }

        holder.tvProjectTitle.setText(project.getProjectTitle());
        holder.tvCompanyName.setText(project.getCopmpanyName());
        holder.tvDuration.setText(project.getDuration());
        holder.tvLink.setText(project.getRefLink());
        holder.tvRole.setText(project.getRole());
        holder.tvDescription.setText(project.getDescription());

        Glide.with(context).load(project.getImgUrl()).asBitmap().placeholder(R.drawable.thumb_personel).centerCrop().into
                (new BitmapImageViewTarget(holder.ivPic) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        holder.ivPic.setImageDrawable(circularBitmapDrawable);

                    }
                });

        holder.tvEdit.setVisibility(View.GONE);
      /*  holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                *//*lastSelectedItem = isExpanded ? -1:position;
                notifyItemChanged(previousExpandedPosition);
                notifyItemChanged(position);*//*
            }
        });*/
    }

    public int getLastSelectedItem() {
        return lastSelectedItem;
    }

    public void setLastSelectedItem(int lastSelectedItem) {
        this.lastSelectedItem = lastSelectedItem;
    }

    class DataViewHolder extends BaseRecyclerAdapter.ViewHolder {

        @BindView(R.id.linDescription)
        LinearLayout linDescription;

        @BindView(R.id.ivPic)
        ImageView ivPic;

        @BindView(R.id.tvProjectTitle)
        TextView tvProjectTitle;

        @BindView(R.id.tvCompanyName)
        TextView tvCompanyName;

        @BindView(R.id.tvLink)
        TextView tvLink;

        @BindView(R.id.tvRole)
        TextView tvRole;

        @BindView(R.id.tvDescription)
        TextView tvDescription;

        @BindView(R.id.tvDuration)
        TextView tvDuration;

        @BindView(R.id.tvEdit)
        TextView tvEdit;

        @BindView(R.id.tvDetails)
        TextView tvDetails;



        public DataViewHolder(View itemView) {
            super(itemView);
            clickableViews(itemView);
            clickableViews(tvDetails);

            getAdapterPosition();

        }
    }
}
