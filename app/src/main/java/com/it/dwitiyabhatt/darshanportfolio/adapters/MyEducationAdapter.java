package com.it.dwitiyabhatt.darshanportfolio.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.it.dwitiyabhatt.darshanportfolio.baseclasses.BaseRecyclerAdapter;
import com.it.dwitiyabhatt.darshanportfolio.models.Education;
import com.it.dwitiyabhatt.darshanportfolio.R;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by user15 on 9/11/17.
 */

public class MyEducationAdapter
        extends BaseRecyclerAdapter<MyEducationAdapter.DataViewHolder, Education> {


    private ArrayList<Education> educationArrayList;
    private Context context;
    private boolean singleSelection=true;
    private int lastSelectedPosition = 0;

    public MyEducationAdapter(Context context, ArrayList<Education> educationArrayList) {
        super(educationArrayList);
        this.educationArrayList = educationArrayList;
        this.context = context;

    }

    public ArrayList<Education> getNavItemArrayList() {
        return educationArrayList;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DataViewHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_my_carrer_path,
                parent, false));
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, final int position) {

        Education education = educationArrayList.get(position);
        holder.tvCourseVal.setText(education.getCourseName());
        holder.tvDurationVal.setText(education.getDuration());
        holder.tvResultVal.setText(education.getResult());
        Glide.with(context).load(education.getImgUrl()).into(holder.ivCertificate);

    }

    class DataViewHolder extends BaseRecyclerAdapter.ViewHolder {

        @BindView(R.id.ivCertificate)
        ImageView ivCertificate;

        @BindView(R.id.tvCourseVal)
        TextView tvCourseVal;

        @BindView(R.id.tvDurationVal)
        TextView tvDurationVal;

        @BindView(R.id.tvResultVal)
        TextView tvResultVal;


        public DataViewHolder(View itemView) {
            super(itemView);
            clickableViews(itemView);

        }
    }

    public int getLastSelectedPosition() {
        return lastSelectedPosition;
    }

    public void setLastSelectedPosition(int lastSelectedPosition) {
        this.lastSelectedPosition = lastSelectedPosition;
    }
}

