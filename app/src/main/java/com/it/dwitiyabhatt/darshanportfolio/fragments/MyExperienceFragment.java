package com.it.dwitiyabhatt.darshanportfolio.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.it.dwitiyabhatt.darshanportfolio.MyPortFolioApp;
import com.it.dwitiyabhatt.darshanportfolio.activities.HomeActivity;
import com.it.dwitiyabhatt.darshanportfolio.adapters.MyEducationAdapter;
import com.it.dwitiyabhatt.darshanportfolio.baseclasses.BaseFragment;
import com.it.dwitiyabhatt.darshanportfolio.baseclasses.BaseRecyclerAdapter;
import com.it.dwitiyabhatt.darshanportfolio.models.Education;
import com.it.dwitiyabhatt.darshanportfolio.utils.Constants;
import com.it.dwitiyabhatt.darshanportfolio.utils.Util;
import com.it.dwitiyabhatt.darshanportfolio.R;

import java.util.ArrayList;
import java.util.Iterator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyExperienceFragment extends BaseFragment {

    @BindView(R.id.recyclerView)
    RecyclerView rvCourses;

    @BindView(R.id.fabAdd)
    FloatingActionButton fabAdd;

    @BindView(R.id.frProgress)
    FrameLayout frProgress;

    @BindView(R.id.tvNoData)
    TextView tvNoData;

    private ArrayList<Education> educationArrayList = new ArrayList<>();
    private MyEducationAdapter myCarrearAdapter;
    private DatabaseReference mDatabase;

    private Activity mActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflating view layout
        View layout = inflater.inflate(R.layout.fragment_my_projects, container, false);
        ButterKnife.bind(this, layout);
        initToolbar();

        showHideProgress(true,frProgress);

        mDatabase = FirebaseDatabase.getInstance().
                getReference().
                child(Constants.USERS).
                child(((HomeActivity)getActivity()).getLoggedInUserId()).child(Constants.EDUCATION);

        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                educationArrayList.clear();
                myCarrearAdapter.notifyDataSetChanged();

                Iterator<DataSnapshot> iter = dataSnapshot.getChildren().iterator();

                if(iter.hasNext()) {
                    tvNoData.setVisibility(View.GONE);
                    while (iter.hasNext()) {
                        Education education = iter.next().getValue(Education.class);
                        educationArrayList.add(education);
                    }
                    myCarrearAdapter.notifyDataSetChanged();
                }else{
                    tvNoData.setVisibility(View.VISIBLE);
                }

                showHideProgress(false,frProgress);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        setupAdapter();


        return layout;
    }


    private void setupAdapter() {
        rvCourses.setLayoutManager(new LinearLayoutManager(mActivity));
        myCarrearAdapter = new MyEducationAdapter(mActivity,educationArrayList);
        rvCourses.setAdapter(myCarrearAdapter.setRecycleOnItemClickListener(mRecycleOnItemClickListener));
    }

    private BaseRecyclerAdapter.RecycleOnItemClickListener mRecycleOnItemClickListener = new BaseRecyclerAdapter.RecycleOnItemClickListener() {
        @Override
        public void onItemClick(View view, final int position)
        {
            Education education = educationArrayList.get(position);

            DetailCarrerFragment detailCarrerFragment =new DetailCarrerFragment();

            Bundle b = new Bundle();
            b.putString("url",education.getImgUrl());

            detailCarrerFragment.setArguments(b);

            Util.addNextFragment(mActivity,detailCarrerFragment, MyExperienceFragment.this,false);
        }
    };

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden)initToolbar();
    }

    public void initToolbar() {
        mActivity = MyPortFolioApp.getmInstance().getActivity();

            ((HomeActivity) mActivity).
                    setUpToolbar(getString(R.string.my_education), true,false);



    }

    @OnClick({R.id.fabAdd})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fabAdd:
                Util.addNextFragment(mActivity,new AddCarrearFragment(), MyExperienceFragment.this,false);
                break;
        }
    }





}
