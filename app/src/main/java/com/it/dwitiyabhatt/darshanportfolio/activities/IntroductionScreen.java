package com.it.dwitiyabhatt.darshanportfolio.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.it.dwitiyabhatt.darshanportfolio.MyPortFolioApp;
import com.it.dwitiyabhatt.darshanportfolio.models.Company;
import com.it.dwitiyabhatt.darshanportfolio.models.Education;
import com.it.dwitiyabhatt.darshanportfolio.models.Project;
import com.it.dwitiyabhatt.darshanportfolio.models.User;
import com.it.dwitiyabhatt.darshanportfolio.utils.Constants;
import com.it.dwitiyabhatt.darshanportfolio.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.it.dwitiyabhatt.darshanportfolio.utils.Constants.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE;

public class IntroductionScreen extends AppCompatActivity {

    @BindView(R.id.rvParent)
    RelativeLayout rvParent;

    private DatabaseReference mDatabase;

    private final int RC_SIGN_IN = 234;

    //Tag for the logs optional
    private static final String TAG = "introductionScreen";

    //creating a GoogleSignInClient object
    GoogleSignInClient mGoogleSignInClient;

    private AlertDialog alert;

    //And also a Firebase Auth object
    FirebaseAuth mAuth;

    private  DatabaseReference myRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_intro_screen);
        ButterKnife.bind(this);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        //setProjectsData();
        //setupCompanyData();
        //setupEducationData();
        AlphaAnimation animation = new AlphaAnimation(0.0f , 1.0f ) ;
        animation.setFillAfter(true);
        animation.setDuration(1200);

        rvParent.startAnimation(animation);

       /* GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mAuth = FirebaseAuth.getInstance();*/
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                // ...
            }
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            //updateUI(user);
                            DatabaseReference myRef = mDatabase.getDatabase().getReference(Constants.USERS);
                            final User modelUser = new User();
                            modelUser.setuId(user.getUid());
                            modelUser.setEmail(user.getEmail());
                            modelUser.setImgUrl(user.getPhotoUrl().toString());
                            modelUser.setName(user.getDisplayName());
                            myRef.child(modelUser.getuId()).child(Constants.USER_DETAIL).setValue(modelUser)
                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    MyPortFolioApp.getmInstance().savePreferenceDataString
                                            (getString(R.string.preference_param_user_is_logged_in),
                                                    "1");
                                    MyPortFolioApp.getmInstance().savePreferenceDataString
                                            (getString(R.string.preference_param_user_id),
                                                    modelUser.getuId());

                                    MyPortFolioApp.getmInstance().savePreferenceDataString
                                            (getString(R.string.preference_param_user_email),
                                                    modelUser.getEmail());

                                    MyPortFolioApp.getmInstance().savePreferenceDataString
                                            (getString(R.string.preference_param_user_name),
                                                    modelUser.getName());

                                    MyPortFolioApp.getmInstance().savePreferenceDataString
                                            (getString(R.string.preference_param_user_img),
                                                    modelUser.getImgUrl());


                                    redirectHomeActivity();
                                }
                            });


                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            //Snackbar.make(findViewById(R.id.main_layout), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                            //updateUI(null);
                        }

                        // ...
                    }
                });
    }

    private void redirectHomeActivity() {
        Toast.makeText(IntroductionScreen.this,
                R.string.message_login_success, Toast.LENGTH_SHORT).show();
        startActivity(new Intent(IntroductionScreen.this,HomeActivity.class));

        finish();
    }


    @OnClick({R.id.tvGetStarted})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.tvGetStarted:
                if(checkPermission(IntroductionScreen.this)){
                    gotoHomeActivity();
                }

                //signIn();
                break;

        }

    }

    private void gotoHomeActivity() {
        if (((ConnectivityManager) getSystemService
                (Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null) {
            MyPortFolioApp.getmInstance().savePreferenceDataString
                    (getString(R.string.preference_param_user_is_logged_in),
                            "1");

            MyPortFolioApp.getmInstance().savePreferenceDataString
                    (getString(R.string.preference_param_user_id),
                            "abcde");

            Intent intent = new Intent(IntroductionScreen.this,HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }else{
            Toast.makeText(this, "Please check your internet", Toast.LENGTH_SHORT).show();
        }

    }


    private void shortcutAdd(String name, int number) {
        // Intent to be send, when shortcut is pressed by user ("launched")
        Intent shortcutIntent = new Intent(getApplicationContext(), IntroductionScreen.class);
        shortcutIntent.setAction(Constants.ACTION_PLAY);

        // Create bitmap with number in it -> very default. You probably want to give it a more stylish look
        Bitmap bitmap = Bitmap.createBitmap(100, 100, Bitmap.Config.ARGB_8888);
        Paint paint = new Paint();
        paint.setColor(0xFF808080); // gray
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize(50);
        new Canvas(bitmap).drawText(""+number, 50, 50, paint);
        ((ImageView) findViewById(R.id.icon)).setImageBitmap(bitmap);

        // Decorate the shortcut
        Intent addIntent = new Intent();
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, name);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON, bitmap);

        // Inform launcher to create shortcut
        addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        getApplicationContext().sendBroadcast(addIntent);
    }

    private void shortcutDel(String name) {
        // Intent to be send, when shortcut is pressed by user ("launched")
        Intent shortcutIntent = new Intent(getApplicationContext(), IntroductionScreen.class);
        shortcutIntent.setAction(Constants.ACTION_PLAY);

        // Decorate the shortcut
        Intent delIntent = new Intent();
        delIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        delIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, name);

        // Inform launcher to remove shortcut
        delIntent.setAction("com.android.launcher.action.UNINSTALL_SHORTCUT");
        getApplicationContext().sendBroadcast(delIntent);
    }

    private void setProjectsData(){
        String pushId;

        Project project;

        pushId = mDatabase.push().getKey();
        myRef = mDatabase.getDatabase()
                .getReference(Constants.USERS)
                .child("abcde").child(Constants.PROJECTS).child(pushId);

        project = new Project();
        project.setId(pushId);
        project.setProjectTitle("Wentamashy");
        project.setRole("Developer and Team Lead");
        project.setCopmpanyName("CMExpertise");
        project.setRefLink("https://play.google.com/store/apps/details?id=com.wentamashy");
        project.setDescription("Wentamashy is a saudi based application to manage events." +
                "All users, vendors and event management volunteers can use this application for their respctive purpose." +
                "Responsibilities :\n" +
                "● Communcate with client and handle bug tracker and feedbacks on a weekly basis" +
                "● Understand coding and stcture of an existing system" +
                "● Lead UI, IOS and back end team and manage project deadlines");
        project.setImgUrl("https://firebasestorage.googleapis.com/v0/b/my-portfolio-3b5f3.appspot.com/o/uploads%2Fmowadcom.png?alt=media&token=f7fbaa0b-9b29-4f0a-a6c7-ce3184748e12");

        myRef.setValue(project);






    }

    private void setupEducationData() {

        String pushId;

        Education education;

        pushId = mDatabase.push().getKey();

        myRef = mDatabase.getDatabase()
                .getReference(Constants.USERS)
                .child("abcde").child(Constants.EDUCATION).child(pushId);
        education = new Education();
        education.setId(pushId);
        education.setCourseName("Bachelor of enginnering");
        education.setDuration("From 2011 To 2014");
        education.setResult("7.16 CGPA");
        education.setImgUrl("https://firebasestorage.googleapis.com/v0/b/my-portfolio-3b5f3.appspot.com/o/uploads%2Fdegree.jpg?alt=media&token=c78725c1-478e-4565-b52f-8f132f5208a2");

        myRef.setValue(education);

        pushId = mDatabase.push().getKey();
        myRef = mDatabase.getDatabase()
                .getReference(Constants.USERS)
                .child("abcde").child(Constants.EDUCATION).child(pushId);
        education = new Education();
        education.setId(pushId);
        education.setCourseName("Diploma of enginnering");
        education.setDuration("From 2008 To 2011");
        education.setResult("7.63 CPI");
        education.setImgUrl("https://firebasestorage.googleapis.com/v0/b/my-portfolio-3b5f3.appspot.com/o/uploads%2Fdiploma.jpg?alt=media&token=5177d2ca-b356-42f5-98a5-a08c29a90285");

        myRef.setValue(education);
    }

    private void setupCompanyData(){

        String pushId;
        Company company;

        pushId = mDatabase.push().getKey();
        myRef = mDatabase.getDatabase()
                .getReference(Constants.USERS)
                .child("abcde").child(Constants.COMPANIES).child(pushId);

        company = new Company();
        company.setId(pushId);
        company.setCompanyName("CMExpertise");
        company.setRole("Application developer");
        company.setWebsiteLink("http://cmexpertiseinfotech.com");
        company.setDescription("Working as an android developer and team leader. Developing applications according to client based requirements.\n" +
                "Here, I worked on latest technologies some of which are following:\n" +
                "● ARKit ( For augmented reality project)\n"  +
                "● Latest android studio and android P OS \n" +
                "● Some project management tools like Pivotal Tracker and Asana\n" +
                "");
        company.setLogo("");                     // If image is not updated, save previous
        company.setDuration("October 2017 to Present");

        myRef.setValue(company);


    }

    /*private void setupCommonData(){
                for(int i =0;i<5;i++){
                    DatabaseReference myRef = mDatabase.getDatabase().getReference(Constants.PROJECTS).child(String.valueOf(i));
                    Project project = new Project();
                    project.setId("1");
                    project.setProjectTitle("Investify");
                    project.setRole("Developer1");
                    project.setCopmpanyName("Pardy Panda Studios");
                    project.setDescription("Description will be added soon...");
                    project.setImgUrl("https://www.siliconeer.com/past_issues/2010/march-2010/NEWS-sachin.jpg");
                  //  project.setFromDate("May 2016");
                   // project.setToDate("August 2016");
                    myRef.setValue(project);

                }

        for(int i =0;i<3;i++){
            DatabaseReference myRef = mDatabase.getDatabase().getReference(Constants.HOBBIES).child(String.valueOf(i));
            CommonDataModel commonDataModel = new CommonDataModel(String.valueOf(i),"Hobbiee "+i,"Nothing to desribe");
            myRef.setValue(commonDataModel);

        }

        for(int i = 0; i<3 ;i++){
            DatabaseReference myRef = mDatabase.getDatabase().getReference(Constants.ACHIVEMENTS).child(String.valueOf(i));
            Achivement achivement = new Achivement(String.valueOf(i),"ACHIVEMENT "+i,"Nothing to desribe");
            achivement.setImgUrl("https://marketplace.canva.com/MAB6okiUZCo/1/0/thumbnail_large/canva-monochromatic-membership-certificate-MAB6okiUZCo.jpg");
            myRef.setValue(achivement);
        }

        for(int i = 0; i<3 ;i++){
            DatabaseReference myRef = mDatabase.getDatabase().getReference(Constants.SKILLS).child(String.valueOf(i));
            CommonDataModel commonDataModel = new CommonDataModel(String.valueOf(i),"Skills "+i,"Nothing to desribe");
            myRef.setValue(commonDataModel);
        }



    }*/

    private boolean checkPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(IntroductionScreen.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE) !=
                    PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    showPermissionNeededDialog(context);
                } else {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    private void showPermissionNeededDialog(Context context) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle(R.string.permission_necessary);
        alertBuilder.setMessage(R.string.external_storage_permission_needed);
        alertBuilder.setPositiveButton(getString(R.string.settings), new DialogInterface.OnClickListener() {
            @android.support.annotation.RequiresApi(api = Build.VERSION_CODES.M)
            public void onClick(DialogInterface dialog, int which) {

                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:" + getPackageName()));
                startActivity(intent);
            }
        });


        alert = alertBuilder.create();
        alert.show();


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                try {
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                       gotoHomeActivity();
                    } else {
                        showPermissionNeededDialog(IntroductionScreen.this);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }

    }

}
