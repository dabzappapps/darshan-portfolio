package com.it.dwitiyabhatt.darshanportfolio.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuAdapter;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.it.dwitiyabhatt.darshanportfolio.MyPortFolioApp;
import com.it.dwitiyabhatt.darshanportfolio.fragments.AboutThisAppFragment;
import com.it.dwitiyabhatt.darshanportfolio.fragments.ContactMeFragment;
import com.it.dwitiyabhatt.darshanportfolio.fragments.MoreAboutMeFragment;
import com.it.dwitiyabhatt.darshanportfolio.fragments.MyCompaniesFragment;
import com.it.dwitiyabhatt.darshanportfolio.fragments.MyEducationFragment;
import com.it.dwitiyabhatt.darshanportfolio.fragments.MyProjectsFragment;
import com.it.dwitiyabhatt.darshanportfolio.fragments.MyResumeFragment;
import com.it.dwitiyabhatt.darshanportfolio.fragments.SettingsFragment;
import com.it.dwitiyabhatt.darshanportfolio.R;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle actionBarDrawerToggle;
    private Handler mHandler;
    private FragmentManager fragmentManager;
    private MenuAdapter mMenuAdapter;
    private Fragment mFragment = null;
    private View headerView;
    private String loggedInUserId;
    private String imgUrl,name,email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       setContentView(R.layout.activity_home);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        MyPortFolioApp.getmInstance().setActivity(this);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        loggedInUserId = MyPortFolioApp.getmInstance().getSharedPreferences().
                getString(getString((R.string.preference_param_user_id)), "");

        imgUrl = MyPortFolioApp.getmInstance().getSharedPreferences().
                getString(getString((R.string.preference_param_user_img)), "");

        name = MyPortFolioApp.getmInstance().getSharedPreferences().
                getString(getString((R.string.preference_param_user_name)), "");

        email = MyPortFolioApp.getmInstance().getSharedPreferences().
                getString(getString((R.string.preference_param_user_email)), "");

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mFragment = new MyResumeFragment();
        openFragment(mFragment);

        headerView = navigationView.inflateHeaderView(R.layout.nav_header_home);
        final ImageView imageView = headerView.findViewById(R.id.imageView);
        TextView tvName = headerView.findViewById(R.id.tvName);
        TextView tvEmail = headerView.findViewById(R.id.tvEmail);

        tvEmail.setText("bhattdarshan13491@gmail.com");
        tvName.setText("Darshan Bhatt");

        Glide.with(HomeActivity.this).load(R.drawable.pro_pic).asBitmap().
                placeholder(R.drawable.thumb_personel).centerCrop().into
                (new BitmapImageViewTarget(imageView) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory
                                                                      .create(HomeActivity.this.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        imageView.setImageDrawable(circularBitmapDrawable);
                    }
                });

    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    public String getLoggedInUserId() {
        return loggedInUserId;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        int id = item.getItemId();

        if (id == android.R.id.home) {

            fragmentManager = getFragmentManager();

            if (fragmentManager.getBackStackEntryCount() > 0) {
                   getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                   return true;
                }
            else {
                drawerLayout.openDrawer(GravityCompat.START);
            }

             }
           return super.onOptionsItemSelected(item);
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

       switch (id){
           case R.id.nav_my_resume:
               mFragment = new MyResumeFragment();
               openFragment(mFragment);
               setToolbartitle(getString(R.string.my_resume_pdf));
               break;
           case R.id.nav_my_companies:
               mFragment = new MyCompaniesFragment();
               openFragment(mFragment);
               setToolbartitle(getString(R.string.nav_my_companies));
               break;
           case R.id.nav_my_carrear_path:
               mFragment = new MyEducationFragment();
               openFragment(mFragment);
               setToolbartitle(getString(R.string.my_education));
               break;
           case R.id.nav_my_projects:
               mFragment = new MyProjectsFragment();
               openFragment(mFragment);
               setToolbartitle(getString(R.string.projects_done));
               break;
           case R.id.nav_more_about_me:
               mFragment = new MoreAboutMeFragment();
               openFragment(mFragment);
               setToolbartitle(getString(R.string.more_about_me));
               break;

           case R.id.nav_about_app:
               mFragment = new AboutThisAppFragment();
               openFragment(mFragment);
               setToolbartitle(getString(R.string.about_app));
               break;
           case R.id.nav_settings:
               mFragment = new SettingsFragment();
               openFragment(mFragment);
               setToolbartitle(getString(R.string.settings));
               break;

           case R.id.nav_contact_me:
               mFragment = new ContactMeFragment();
               openFragment(mFragment);
               setToolbartitle(getString(R.string.contact_me));
               break;

           case R.id.nav_share:
               shareApp();
               break;
           }


        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void shareApp() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "Hey check out my personalised app on play store! : https://play.google.com/store/apps/details?id=com.it.dwitiyabhatt.darshanportfolio");
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }


    private void setToolbartitle(String title){
        toolbar.setTitle(title);
    }



    private Fragment getHomeFragment() {

                MyResumeFragment homeFragment = new MyResumeFragment();
                return homeFragment;

        }

    private void openFragment(final Fragment mFragment) {
        final android.app.FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frame, mFragment, mFragment.getClass().getSimpleName());
        transaction.commit();
        drawerLayout.closeDrawer(GravityCompat.START);

    }

    public void setUpToolbar(final String title,boolean showToolbar, final boolean isShowback) {

        toolbar.setVisibility(showToolbar?View.VISIBLE:View.GONE);

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(isShowback ? R.drawable.ic_back : R.drawable.ic_menu);
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }




}




