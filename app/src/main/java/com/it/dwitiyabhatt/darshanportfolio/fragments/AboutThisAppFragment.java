package com.it.dwitiyabhatt.darshanportfolio.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.it.dwitiyabhatt.darshanportfolio.R;

import butterknife.ButterKnife;

public class AboutThisAppFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflating view layout
        View layout = inflater.inflate(R.layout.fragment_about_this_app, container, false);
        ButterKnife.bind(this, layout);

        return layout;
    }

}
