package com.it.dwitiyabhatt.darshanportfolio.models;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Project {

    private String id,imgUrl,projectTitle,copmpanyName,duration,refLink,role,description;

    public Project() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getProjectTitle() {
        return projectTitle;
    }

    public void setProjectTitle(String projectTitle) {
        this.projectTitle = projectTitle;
    }

    public String getCopmpanyName() {
        return copmpanyName;
    }

    public void setCopmpanyName(String copmpanyName) {
        this.copmpanyName = copmpanyName;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getRefLink() {
        return refLink;
    }

    public void setRefLink(String refLink) {
        this.refLink = refLink;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
