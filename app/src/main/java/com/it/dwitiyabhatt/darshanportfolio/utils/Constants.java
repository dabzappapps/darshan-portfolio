package com.it.dwitiyabhatt.darshanportfolio.utils;

public class Constants {
    public static final String ACTION_PLAY="action_play";

    //FirebaseConstants
    public static final String USER_ID = "abcde";
    public static final String PROJECTS = "projects";
    public static final String USERS = "users";
    public static final String QUERIES = "queries";
    public static final String EDUCATION = "educations";
    public static final String COMPANIES = "companies";
    public static final String USER_DETAIL = "user_details";
    public static final String SKILLS = "skills";
    public static final String ACHIVEMENTS = "achivements";
    public static final String HOBBIES= "hobbies";


    //Storage directory constants
    public static final String MAIN_DIRECTORY = "Darshan portfolio docs/";
    public static final String RESUME_DIRECTORY = "Resume";

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;

    public static final String STORAGE_PATH_UPLOADS = "uploads/";
    public static final String DATABASE_PATH_UPLOADS = "uploads";
    public static  boolean IS_HOME= false;
    public static final int PICK_IMAGE_REQUEST = 234;
    public static final int UPLOAD_PDF = 235;
}
