package com.it.dwitiyabhatt.darshanportfolio.models;

public class Achivement extends CommonDataModel {


    private String imgUrl;

    public Achivement(String id, String title, String descr) {
        super(id, title, descr);
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }
}
