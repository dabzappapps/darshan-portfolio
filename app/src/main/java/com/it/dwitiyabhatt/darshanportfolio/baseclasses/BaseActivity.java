package com.it.dwitiyabhatt.darshanportfolio.baseclasses;

import android.app.Activity;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


/**
 * Created by user15 on 21/11/17.
 */

public class BaseActivity extends AppCompatActivity {



    private ProgressBar progressBar;
    private  RelativeLayout layout;
    private android.support.v7.app.AlertDialog dialog;

    public void showToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }


    public void setProgressBar(Activity activity){
        if(activity !=null){
            progressBar = new ProgressBar(activity,null,android.R.attr.progressBarStyleLarge);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(100,100);

            params.addRule(RelativeLayout.CENTER_IN_PARENT);
            //relMain.addView(progressBar,params);
            progressBar.setVisibility(View.GONE);
        }

    }



     public void showSnackbar(boolean isSnakbar,View parent,String msg){
         try
         {
             if (isSnakbar)
             {
                 Snackbar snack = Snackbar.make(parent, msg, Snackbar.LENGTH_LONG);
                 //snack.getView().setBackgroundColor(Color.parseColor("#FF7043"));
                 View viewNew = snack.getView();
                 TextView tv = (TextView) viewNew.findViewById(android.support.design.R.id.snackbar_text);
                 tv.setGravity(Gravity.CENTER_HORIZONTAL);
                 snack.show();
             } else {
                 Toast.makeText(getApplicationContext(),msg, Toast.LENGTH_LONG).show();
             }
         }
         catch (Exception e){e.printStackTrace();}

     }

     public void showHideProgress(boolean isShowProgress, FrameLayout progressBar) {
        if (isShowProgress) {
            progressBar.setVisibility(View.VISIBLE);
            progressBar.setEnabled(false);

        } else {
            progressBar.setVisibility(View.GONE);
            progressBar.setEnabled(true);
        }

    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View v = getCurrentFocus();
        return super.dispatchTouchEvent(ev);
    }

  /*  public void showHideProgress(boolean isShowProgress) {
        if (isShowProgress) {
            LayoutInflater inflater = getLayoutInflater();
            progressBar = (ProgressBar ) inflater.inflate(R.layout.progress_bar, null);
            progressBar.setVisibility(View.VISIBLE);

        } else {
            progressBar.setVisibility(View.GONE);
        }

    }*/

}
