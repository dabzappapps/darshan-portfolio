package com.it.dwitiyabhatt.darshanportfolio.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.it.dwitiyabhatt.darshanportfolio.MyPortFolioApp;
import com.it.dwitiyabhatt.darshanportfolio.activities.HomeActivity;
import com.it.dwitiyabhatt.darshanportfolio.models.Query;
import com.it.dwitiyabhatt.darshanportfolio.utils.Constants;
import com.it.dwitiyabhatt.darshanportfolio.utils.Util;
import com.it.dwitiyabhatt.darshanportfolio.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ContactMeFragment extends Fragment {

    @BindView(R.id.ivPic)
    ImageView ivPic;

    private AlertDialog alertDialog;

    private DatabaseReference mDatabase;
    private  DatabaseReference myRef;

    private String pushId="";
    
    private Activity mActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflating view layout
        View layout = inflater.inflate(R.layout.fragment_contact_me, container, false);
        ButterKnife.bind(this, layout);
        
        initToolbar();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        pushId= mDatabase.push().getKey();
        myRef = mDatabase.getDatabase()
                .getReference(Constants.QUERIES);

        Glide.with(mActivity).load(R.drawable.pro_pic).asBitmap().
                placeholder(R.drawable.thumb_personel).centerCrop().into
                (new BitmapImageViewTarget(ivPic) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory
                                .create(mActivity.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        ivPic.setImageDrawable(circularBitmapDrawable);
                    }
                });


        return layout;
    }

    public void initToolbar() {

        mActivity = MyPortFolioApp.getmInstance().getActivity();
        ((HomeActivity) mActivity).
                setUpToolbar(mActivity.getString(R.string.contact_me), true,false);



    }

    @OnClick({R.id.floatMessage})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.floatMessage:
                openMessageDialog();
                break;

        }

    }

    private void openMessageDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle(R.string.enter_your_query);
        builder.setView(R.layout.fragment_create_same_app);
        alertDialog = builder.create();
        alertDialog.show();
        final EditText etName =  alertDialog.findViewById(R.id.etName);
        final EditText etEmail =  alertDialog.findViewById(R.id.etEmail);
        final EditText etRequirements =  alertDialog.findViewById(R.id.etRequirements);
        FloatingActionButton fabSend = alertDialog.findViewById(R.id.fabSend);

        fabSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(etName.getText().toString().trim().isEmpty()){
                    Toast.makeText(mActivity, R.string.name_validation, Toast.LENGTH_SHORT).show();
                } else if(etEmail.getText().toString().trim().isEmpty()){
                    Toast.makeText(mActivity, R.string.contact_validation, Toast.LENGTH_SHORT).show();
                }else if(etRequirements.getText().toString().trim().isEmpty()){
                    Toast.makeText(mActivity, R.string.requirements_validation, Toast.LENGTH_SHORT).show();
                }
                else{
                    Query query = new Query();
                    query.setId(pushId);
                    query.setName(Util.getInputText(etName));
                    query.setContactLink(Util.getInputText(etEmail));
                    query.setRequirements(Util.getInputText(etRequirements));
                    myRef.child(pushId).setValue(query);

                    alertDialog.dismiss();
                    Toast.makeText(mActivity, R.string.thank_you_msg, Toast.LENGTH_SHORT).show();
                }

            }
        });




    }
}