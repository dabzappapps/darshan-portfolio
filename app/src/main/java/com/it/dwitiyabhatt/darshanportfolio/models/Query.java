package com.it.dwitiyabhatt.darshanportfolio.models;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Query {

    private String id,name,contactLink,requirements;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactLink() {
        return contactLink;
    }

    public void setContactLink(String contactLink) {
        this.contactLink = contactLink;
    }

    public String getRequirements() {
        return requirements;
    }

    public void setRequirements(String requirements) {
        this.requirements = requirements;
    }
}
