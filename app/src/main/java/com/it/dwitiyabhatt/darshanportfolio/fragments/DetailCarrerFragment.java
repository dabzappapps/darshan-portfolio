package com.it.dwitiyabhatt.darshanportfolio.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.it.dwitiyabhatt.darshanportfolio.MyPortFolioApp;
import com.it.dwitiyabhatt.darshanportfolio.activities.HomeActivity;
import com.it.dwitiyabhatt.darshanportfolio.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailCarrerFragment extends Fragment {

    @BindView(R.id.ivCertificate)
    ImageView ivCertificate;

    private String url="";

    private Activity mActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflating view layout
        View layout = inflater.inflate(R.layout.fragment_detail, container, false);
        ButterKnife.bind(this, layout);
        initToolbar();

        Bundle b = getArguments();
        url = b.getString("url");

       // ivCertificate.setImageURI(Uri.parse(url));
       // Glide.with(getActivity()).load(url).into(ivCertificate).onLoadFailed(new Ex);

        Glide.with(getActivity()).load(url).listener(new RequestListener<String, GlideDrawable>() {
            @Override
            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                e.printStackTrace();
                return true;
            }

            @Override
            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                return false;
            }
        }).into(ivCertificate);


        return layout;
    }

    public void initToolbar() {

        mActivity = MyPortFolioApp.getmInstance().getActivity();
            ((HomeActivity) mActivity).
                    setUpToolbar(mActivity.getString(R.string.certification), true,true);



    }



}