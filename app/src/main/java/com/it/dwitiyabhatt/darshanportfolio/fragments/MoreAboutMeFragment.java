package com.it.dwitiyabhatt.darshanportfolio.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.it.dwitiyabhatt.darshanportfolio.activities.HomeActivity;
import com.it.dwitiyabhatt.darshanportfolio.adapters.AchivementsAdapter;
import com.it.dwitiyabhatt.darshanportfolio.adapters.HobbieesAdapter;
import com.it.dwitiyabhatt.darshanportfolio.baseclasses.BaseRecyclerAdapter;
import com.it.dwitiyabhatt.darshanportfolio.models.SingleSelectionModel;
import com.it.dwitiyabhatt.darshanportfolio.utils.Util;
import com.it.dwitiyabhatt.darshanportfolio.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MoreAboutMeFragment extends Fragment {

    @BindView(R.id.rvHobbies)
    RecyclerView rvHobbies;

    @BindView(R.id.rvSkills)
    RecyclerView rvSkills;

    private ArrayList<SingleSelectionModel> singleSelectionModelList = new ArrayList<>();
    private HobbieesAdapter hobbieesAdapter;
    private AchivementsAdapter achivementsAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflating view layout
        View layout = inflater.inflate(R.layout.fragment_more_about_me, container, false);
        ButterKnife.bind(this, layout);

        setupAdapter();

        fillDummyData();


        return layout;
    }

    private void fillDummyData() {
        try {
            for(int i =0;i<6;i++){
                singleSelectionModelList.add(new SingleSelectionModel());
            }
            hobbieesAdapter.notifyDataSetChanged();
            achivementsAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setupAdapter() {
        try {
            rvHobbies.setLayoutManager(new LinearLayoutManager(getActivity()));
            rvSkills.setLayoutManager(new LinearLayoutManager(getActivity()));
            hobbieesAdapter = new HobbieesAdapter(getActivity(),singleSelectionModelList);
            achivementsAdapter = new AchivementsAdapter(getActivity(),singleSelectionModelList);
            rvHobbies.setAdapter(hobbieesAdapter);
            rvSkills.setAdapter(achivementsAdapter.setRecycleOnItemClickListener(mRecycleAchivementClickListener));
            initToolbar();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private BaseRecyclerAdapter.RecycleOnItemClickListener mRecycleAchivementClickListener = new BaseRecyclerAdapter.RecycleOnItemClickListener() {
        @Override
        public void onItemClick(View view, final int position)
        {
            Util.addNextFragment(getActivity(),new AchivementsDetailFragment(), MoreAboutMeFragment.this,false);
        }
    };


    private BaseRecyclerAdapter.RecycleOnItemClickListener mRecycleOnItemClickListener = new BaseRecyclerAdapter.RecycleOnItemClickListener() {
        @Override
        public void onItemClick(View view, final int position)
        {

        }
    };

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if(!hidden)initToolbar();
    }

    public void initToolbar() {

        if (getActivity() != null)
        {
            ((HomeActivity) getActivity()).
                    setUpToolbar(getString(R.string.achivements), true,false);
        }


    }

}
