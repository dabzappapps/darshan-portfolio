package com.it.dwitiyabhatt.darshanportfolio.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.it.dwitiyabhatt.darshanportfolio.baseclasses.BaseRecyclerAdapter;
import com.it.dwitiyabhatt.darshanportfolio.models.Company;
import com.it.dwitiyabhatt.darshanportfolio.R;

import java.util.ArrayList;

import butterknife.BindView;

public class MyCompaniesAdapter extends BaseRecyclerAdapter<MyCompaniesAdapter.DataViewHolder, Company> {


    private ArrayList<Company> companyArrayList;
    private Context context;
    private int lastSelectedItem = -1;


    public MyCompaniesAdapter(Context context, ArrayList<Company> companyArrayList) {
        super(companyArrayList);
        this.companyArrayList = companyArrayList;
        this.context = context;


    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DataViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_companies,
                parent, false));
    }

    @Override
    public void onBindViewHolder(final DataViewHolder holder, final int position) {
        Company company = companyArrayList.get(position);

        if(lastSelectedItem == position){
            holder.linDescription.setVisibility(View.VISIBLE);
            holder.itemView.setActivated(true);
        }else{
            holder.linDescription.setVisibility(View.GONE);
            holder.itemView.setActivated(true);
        }


        holder.tvCompanyName.setText(company.getCompanyName());
        holder.tvDuration.setText(company.getDuration());
        holder.tvLink.setText(company.getWebsiteLink());
        holder.tvPosition.setText(company.getRole());
        holder.tvDescription.setText(company.getDescription());

       /* Glide.with(context).load(company.getLogo()).asBitmap().placeholder(R.drawable.thumb_personel).centerCrop().into
                (new BitmapImageViewTarget(holder.ivPic) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        holder.ivPic.setImageDrawable(circularBitmapDrawable);

                    }
                });*/
       holder.ivPic.setVisibility(View.GONE);
       holder.tvEdit.setVisibility(View.GONE);

 }
    public int getLastSelectedItem() {
        return lastSelectedItem;
    }

    public void setLastSelectedItem(int lastSelectedItem) {
        this.lastSelectedItem = lastSelectedItem;
    }

    class DataViewHolder extends BaseRecyclerAdapter.ViewHolder {

        @BindView(R.id.linDescription)
        LinearLayout linDescription;

        @BindView(R.id.ivPic)
        ImageView ivPic;


        @BindView(R.id.tvCompanyName)
        TextView tvCompanyName;

        @BindView(R.id.tvLink)
        TextView tvLink;

        @BindView(R.id.tvPosition)
        TextView tvPosition;

        @BindView(R.id.tvDescription)
        TextView tvDescription;

        @BindView(R.id.tvDuration)
        TextView tvDuration;

        @BindView(R.id.tvEdit)
        TextView tvEdit;

        @BindView(R.id.tvDetails)
        TextView tvDetails;

        @BindView(R.id.linRole)
        LinearLayout linRole;



        public DataViewHolder(View itemView) {
            super(itemView);
  //          clickableViews(itemView);
            clickableViews(tvDetails);
            clickableViews(tvEdit);
            getAdapterPosition();

        }
    }
}
