package com.it.dwitiyabhatt.darshanportfolio.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.it.dwitiyabhatt.darshanportfolio.baseclasses.BaseRecyclerAdapter;
import com.it.dwitiyabhatt.darshanportfolio.models.SingleSelectionModel;
import com.it.dwitiyabhatt.darshanportfolio.R;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by user15 on 9/11/17.
 */

public class LanguageSelectionAdapter
        extends BaseRecyclerAdapter<LanguageSelectionAdapter.DataViewHolder, SingleSelectionModel> {


    private ArrayList<SingleSelectionModel> categoryArrayList;
    private Context context;
    private boolean singleSelection=true;
    private int lastSelectedPosition = 0;

    public LanguageSelectionAdapter(Context context, ArrayList<SingleSelectionModel> categoryArrayList) {
        super(categoryArrayList);
        this.categoryArrayList = categoryArrayList;
        this.context = context;

    }

    public LanguageSelectionAdapter(ArrayList<SingleSelectionModel> categoryArrayList, Context context, boolean singleSelection) {
        super(categoryArrayList);
        this.categoryArrayList = categoryArrayList;
        this.context = context;
        this.singleSelection = singleSelection;
    }

    public ArrayList<SingleSelectionModel> getNavItemArrayList() {
        return categoryArrayList;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DataViewHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_single_selection,
                parent, false));
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, final int position) {

        SingleSelectionModel singleSelectionModel = categoryArrayList.get(position);
        holder.tvTitle.setText(singleSelectionModel.getTitle());

            if(lastSelectedPosition == position){
                holder.radioSelection.setChecked(true);
            }else {
                holder.radioSelection.setChecked(false);
            }


    }

    class DataViewHolder extends BaseRecyclerAdapter.ViewHolder {

        @BindView(R.id.radioSelection)
        public RadioButton radioSelection;

        @BindView(R.id.tvTitle)
        public TextView tvTitle;

        @BindView(R.id.rvParent)
        public RelativeLayout rvParent;


        public DataViewHolder(View itemView) {
            super(itemView);
            clickableViews(itemView);
            clickableViews(rvParent);
            }
    }

    public int getLastSelectedPosition() {
        return lastSelectedPosition;
    }

    public void setLastSelectedPosition(int lastSelectedPosition) {
        this.lastSelectedPosition = lastSelectedPosition;
    }
}

