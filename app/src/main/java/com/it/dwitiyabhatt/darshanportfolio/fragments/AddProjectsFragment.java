package com.it.dwitiyabhatt.darshanportfolio.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.it.dwitiyabhatt.darshanportfolio.MyPortFolioApp;
import com.it.dwitiyabhatt.darshanportfolio.activities.HomeActivity;
import com.it.dwitiyabhatt.darshanportfolio.baseclasses.BaseFragment;
import com.it.dwitiyabhatt.darshanportfolio.models.DialogItem;
import com.it.dwitiyabhatt.darshanportfolio.models.Project;
import com.it.dwitiyabhatt.darshanportfolio.utils.Constants;
import com.it.dwitiyabhatt.darshanportfolio.utils.ImageCompression;
import com.it.dwitiyabhatt.darshanportfolio.utils.Util;
import com.it.dwitiyabhatt.darshanportfolio.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;

public class AddProjectsFragment extends BaseFragment {

    @BindView(R.id.ivPic)
    ImageView ivPic;

    @BindView(R.id.etProjectName)
    EditText etProjectName;

    @BindView(R.id.etCompanyName)
    EditText etCompanyName;

    @BindView(R.id.etDuration)
    EditText etDuration;

    @BindView(R.id.etLink)
    EditText etLink;

    @BindView(R.id.etRole)
    EditText etRole;

    @BindView(R.id.etDescription)
    EditText etDescription;

    @BindView(R.id.frProgress)
    FrameLayout frProgress;

    private Activity mActivity;
    
    private final int REQUEST_CAMERA = 0;
    private final int SELECT_FILE = 1;
    private final int IMG_QUALITY = 100;
    public  final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

    private Project project;

    private File destination;
    private  DatabaseReference myRef;
    private String userChoosenTask, profilePic;
    private StorageReference storageReference;
    private DatabaseReference mDatabase;
    private Uri filePath;
    private String pushId="";
    private boolean isEditMode = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflating view layout
        View layout = inflater.inflate(R.layout.fragment_add_project, container, false);
        ButterKnife.bind(this, layout);

        storageReference = FirebaseStorage.getInstance().getReference();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        Bundle b = getArguments();
        if(b !=null){
            isEditMode = true;
            pushId = b.getString("id");
            showHideProgress(true,frProgress);
            myRef = mDatabase.getDatabase()
                    .getReference(Constants.USERS)
                    .child(((HomeActivity)getActivity()).getLoggedInUserId()).child(Constants.PROJECTS).child(pushId);

            myRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    showHideProgress(false,frProgress);
                    project = dataSnapshot.getValue(Project.class);
                    etProjectName.setText(project.getProjectTitle());
                    etCompanyName.setText(project.getCopmpanyName());
                    etDuration.setText(project.getDuration());
                    etLink.setText(project.getRefLink());
                    etRole.setText(project.getRole());
                    etDescription.setText(project.getDescription());

                    Glide.with(mActivity).load(project.getImgUrl()).asBitmap().
                            placeholder(R.drawable.thumb_personel).centerCrop().into
                            (new BitmapImageViewTarget(ivPic) {
                                @Override
                                protected void setResource(Bitmap resource) {
                                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(mActivity.getResources(), resource);
                                    circularBitmapDrawable.setCircular(true);
                                    ivPic.setImageDrawable(circularBitmapDrawable);

                                }
                            });
                }

                @Override
                public void onCancelled(DatabaseError error) {
                    // Failed to read value
                    Log.w("firebase_project", "Failed to read value.", error.toException());
                }
            });
            }
        else{
            pushId= mDatabase.push().getKey();
        }
        initToolbar();

        return layout;

    }

    public void initToolbar() {

        mActivity = MyPortFolioApp.getmInstance().getActivity();
            if(isEditMode){
                ((HomeActivity) mActivity).
                        setUpToolbar(mActivity.getString(R.string.update_project), true,true);
            }else{
                ((HomeActivity) mActivity).
                        setUpToolbar(mActivity.getString(R.string.add_projects), true,true);
            }

        


    }


    @OnClick({R.id.ivPic, R.id.tvSubmit})
    public void onClick(View view){
        switch (view.getId()){
            case R.id.ivPic:
                selectImage();
                break;

            case R.id.tvSubmit:
                if(Util.getInputText(etProjectName).isEmpty()){
                    Toast.makeText(mActivity, R.string.validation_project_name, Toast.LENGTH_SHORT).show();
                    etProjectName.requestFocus();
                }else if(Util.getInputText(etCompanyName).isEmpty()){
                    Toast.makeText(mActivity, R.string.validation_company_name, Toast.LENGTH_SHORT).show();
                    etCompanyName.requestFocus();
                }else if(Util.getInputText(etDuration).isEmpty()){
                    Toast.makeText(mActivity, R.string.validation_project_duration, Toast.LENGTH_SHORT).show();
                    etDuration.requestFocus();
                }else if(Util.getInputText(etRole).isEmpty()){
                    Toast.makeText(mActivity, R.string.validation_project_role, Toast.LENGTH_SHORT).show();
                    etDuration.requestFocus();
                }else if(Util.getInputText(etDescription).isEmpty()){
                    Toast.makeText(mActivity, R.string.validation_project_brief, Toast.LENGTH_SHORT).show();
                    etDuration.requestFocus();
                }
                else {
                    uploadToServer();
                }
                //uploadToServer();
                break;

        }

    }

    private void uploadToServer() {

        showHideProgress(true,frProgress);
        myRef = mDatabase.getDatabase()
                .getReference(Constants.USERS)
                .child(((HomeActivity)getActivity()).getLoggedInUserId()).child(Constants.PROJECTS).child(pushId);

        if(project == null) project = new Project();
        project.setId(pushId);
        project.setProjectTitle(Util.getInputText(etProjectName));
        project.setRole(Util.getInputText(etRole));
        project.setRefLink(Util.getInputText(etLink));
        project.setCopmpanyName(Util.getInputText(etCompanyName));
        project.setDescription(Util.getInputText(etDescription));
        if(!isEditMode)project.setImgUrl("");                     // If image is not updated, save previous
        project.setDuration(Util.getInputText(etDuration));

        myRef.setValue(project).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {

                if(destination !=null){
                    uploadFile(Uri.fromFile(new File(destination.getPath())));
                    }else{
                    showHideProgress(false,frProgress);
                    getFragmentManager().popBackStack();
                }


            }
        });
    }

    private void selectImage() {

        final DialogItem[] items = {
                new DialogItem(getString(R.string.take_photo), R.drawable.camera2),
                new DialogItem(getString(R.string.choose_from_library), R.drawable.gallery)
        };

        ListAdapter adapter = new ArrayAdapter<DialogItem>(
                mActivity,
                R.layout.dialog_select_item,
                R.id.tvTitle,
                items){
            public View getView(int position, View convertView, ViewGroup parent) {
                //Use super class to create the View
                View v = super.getView(position, convertView, parent);
                TextView tv = (TextView)v.findViewById(R.id.tvTitle);
                ImageView ivIcon = v.findViewById(R.id.ivIcon);
                //Put the image on the TextView
                tv.setText(items[position].text);
                ivIcon.setImageResource(items[position].icon);


                return v;
            }
        };



        new AlertDialog.Builder(mActivity)
                .setTitle(R.string.choose)
                .setAdapter(adapter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {
                        boolean result = checkPermission(mActivity);
                        if (item == 0) {
                            userChoosenTask = getString(R.string.take_photo_from_camera);
                            if (result)
                                cameraIntent();
                        } else if (item == 1) {
                            userChoosenTask = getString(R.string.choose_from_library);
                            if (result)
                                galleryIntent();
                        } else if (item == 2) {
                            dialog.dismiss();
                        }
                    }
                }).show();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                    if (requestCode == SELECT_FILE) {
                        filePath = data.getData();
                        onSelectFromGalleryResult(data);
                    }
                    else if (requestCode == REQUEST_CAMERA) {
                        onCaptureImageResult(data);
                    }

                }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public boolean checkPermission(final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) !=
                    PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                    alertBuilder.setCancelable(true);
                    alertBuilder.setTitle(R.string.permission_necessary);
                    alertBuilder.setMessage(R.string.external_storage_permission_needed);
                    alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @android.support.annotation.RequiresApi(api = Build.VERSION_CODES.M)
                        public void onClick(DialogInterface dialog, int which) {
                            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        }
                    });
                    AlertDialog alert = alertBuilder.create();
                    alert.show();
                } else {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    private void cameraIntent() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, REQUEST_CAMERA);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.select_file)), SELECT_FILE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constants.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                try {
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        if (userChoosenTask.equals(getString(R.string.take_photo_from_camera)))
                            cameraIntent();
                        else if (userChoosenTask.equals(getString(R.string.choose_from_library)))
                            galleryIntent();
                    } else {
                        //code for deny
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
        }

    }


    private void onCaptureImageResult(Intent data) throws IOException {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        if (thumbnail != null) {
            thumbnail.compress(Bitmap.CompressFormat.JPEG, IMG_QUALITY, bytes);
            destination = new File(Environment.getExternalStorageDirectory(),
                    System.currentTimeMillis() + ".jpg");

            FileOutputStream fo;
            try {
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            ImageCompression imageCompression =new ImageCompression(mActivity);

        //    uploadFile(Uri.fromFile(new File(destination.getPath())));



            //Glide.with(mActivity).load(destination).placeholder(R.drawable.upload_img_placeholder).into(ivPic);

            Glide.with(mActivity).load(destination).asBitmap().placeholder(R.drawable.thumb_personel).centerCrop().into
                    (new BitmapImageViewTarget(ivPic) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(mActivity.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            ivPic.setImageDrawable(circularBitmapDrawable);

                        }
                    });


        } else {
            Toast.makeText(mActivity, getString(R.string.cantt_capture_image),
                    Toast.LENGTH_SHORT).show();
        }
        //imgFetch.setImageBitmap(thumbnail);
    }


    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                //bm = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), data.getData());
                Bitmap thumbnail = MediaStore.Images.Media.getBitmap(mActivity.getApplicationContext().getContentResolver(), data.getData());
                if (thumbnail != null) {
                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                    destination = new File(Environment.getExternalStorageDirectory(),
                            System.currentTimeMillis() + ".jpg");
                    FileOutputStream fo;
                    try {
                        destination.createNewFile();
                        fo = new FileOutputStream(destination);
                        fo.write(bytes.toByteArray());
                        fo.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                   /* Picasso.with(getContext()).load(destination).placeholder(R.drawable.profilbg).
                            transform(new CircleTransform()).into(ivProfile);*/
                    ImageCompression imageCompression =new ImageCompression(mActivity);
                    String filepath= imageCompression.compressImage(destination.getAbsolutePath());


                  //  uploadFile(filePath);

                    //Glide.with(mActivity).load(destination).placeholder(R.drawable.upload_img_placeholder).into(ivPic);

                    Glide.with(mActivity).load(destination).asBitmap().placeholder(R.drawable.thumb_personel).centerCrop().into
                            (new BitmapImageViewTarget(ivPic) {
                                @Override
                                protected void setResource(Bitmap resource) {
                                    RoundedBitmapDrawable circularBitmapDrawable = RoundedBitmapDrawableFactory.create(mActivity.getResources(), resource);
                                    circularBitmapDrawable.setCircular(true);
                                    ivPic.setImageDrawable(circularBitmapDrawable);

                                }
                            });

                } else {
                    Toast.makeText(mActivity, getString(R.string.cantt_capture_image),
                            Toast.LENGTH_SHORT).show();
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }




    private void uploadFile(Uri filePath) {
        //checking if file is available
        if (filePath != null) {
            //displaying progress dialog while image is uploading
            final ProgressDialog progressDialog = new ProgressDialog(mActivity);
            progressDialog.setTitle("Uploading");
            progressDialog.show();

            //getting the storage reference
            final StorageReference sRef = storageReference.child(Constants.STORAGE_PATH_UPLOADS + System.currentTimeMillis()
                    + ".jpg");

            //adding the file to reference
            sRef.putFile(filePath)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            //dismissing the progress dialog
                            progressDialog.dismiss();

                            //displaying success toast
                            //Toast.makeText(mActivity, "File Uploaded ", Toast.LENGTH_LONG).show();



                            sRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {

                                    project.setImgUrl(uri.toString());
                                    myRef.setValue(project).addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            showHideProgress(false,frProgress);
                                            getFragmentManager().popBackStack();
                                            Toast.makeText(mActivity, "Project details uploaded successfully", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                            });


                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            progressDialog.dismiss();
                            Toast.makeText(mActivity, exception.getCause().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            //displaying the upload progress
                            double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();
                            progressDialog.setMessage("Uploaded " + ((int) progress) + "%...");
                        }
                    });
        } else {
            //display an error if no file is selected
        }
    }


}
