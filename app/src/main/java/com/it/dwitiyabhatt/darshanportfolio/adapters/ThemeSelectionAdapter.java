package com.it.dwitiyabhatt.darshanportfolio.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.it.dwitiyabhatt.darshanportfolio.baseclasses.BaseRecyclerAdapter;
import com.it.dwitiyabhatt.darshanportfolio.models.SingleSelectionModel;
import com.it.dwitiyabhatt.darshanportfolio.R;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by user15 on 9/11/17.
 */

public class ThemeSelectionAdapter
        extends BaseRecyclerAdapter<ThemeSelectionAdapter.DataViewHolder, SingleSelectionModel> {


    private ArrayList<SingleSelectionModel> categoryArrayList;
    private Context context;
    private boolean singleSelection=false;
    private int lastSelectedPosition = 0;

    public ThemeSelectionAdapter(Context context, ArrayList<SingleSelectionModel> categoryArrayList) {
        super(categoryArrayList);
        this.categoryArrayList = categoryArrayList;
        this.context = context;

    }

    public ThemeSelectionAdapter(ArrayList<SingleSelectionModel> categoryArrayList, Context context, boolean singleSelection) {
        super(categoryArrayList);
        this.categoryArrayList = categoryArrayList;
        this.context = context;
        this.singleSelection = singleSelection;
    }

    public ArrayList<SingleSelectionModel> getNavItemArrayList() {
        return categoryArrayList;
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new DataViewHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_single_selection,
                parent, false));
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, final int position) {

        SingleSelectionModel singleSelectionModel = categoryArrayList.get(position);
        holder.radioSelection.setText(singleSelectionModel.getTitle());
        if(singleSelection){
            if(lastSelectedPosition == position){
                holder.radioSelection.setSelected(true);
            }else {
                holder.radioSelection.setSelected(false);
            }
        }else {
            // TODO remove if unnnecessary till the end
            /*if(category.isChecked())
                ImageUtils.loadSimpleImage(holder.ivTick,context,null,R.drawable.right_selected);
            else
                ImageUtils.loadSimpleImage(holder.ivTick,context,null,R.drawable.none_selected);*/
        }

    }

    class DataViewHolder extends BaseRecyclerAdapter.ViewHolder {

        @BindView(R.id.radioSelection)
        public RadioButton radioSelection;


        public DataViewHolder(View itemView) {
            super(itemView);
            clickableViews(itemView);
        }
    }

    public int getLastSelectedPosition() {
        return lastSelectedPosition;
    }

    public void setLastSelectedPosition(int lastSelectedPosition) {
        this.lastSelectedPosition = lastSelectedPosition;
    }
}

