package com.it.dwitiyabhatt.darshanportfolio.activities;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.it.dwitiyabhatt.darshanportfolio.MyPortFolioApp;
import com.it.dwitiyabhatt.darshanportfolio.R;

import java.util.Locale;

public class SplashActivity extends AppCompatActivity  {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        new SplashTask().execute();
    }

    private class SplashTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);



          /*  if(MyPortFolioApp.getmInstance().getSharedPreferences().
                    getString(getString((R.string.preference_param_user_is_logged_in)), "").isEmpty()){
                Intent mLoginIntent = new Intent(SplashActivity.this, IntroductionScreen.class);
                startActivity(mLoginIntent);
                overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out);
                finish();
            }else{
                Intent mLoginIntent = new Intent(SplashActivity.this, HomeActivity.class);
                startActivity(mLoginIntent);
                overridePendingTransition(R.anim.anim_right_in, R.anim.anim_left_out);
                finish();

            }*/

           gotoHomeActivity();

        }
    }

    private void gotoHomeActivity() {
        if (((ConnectivityManager) getSystemService
                (Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null) {
            MyPortFolioApp.getmInstance().savePreferenceDataString
                    (getString(R.string.preference_param_user_is_logged_in),
                            "1");

            MyPortFolioApp.getmInstance().savePreferenceDataString
                    (getString(R.string.preference_param_user_id),
                            "abcde");

            Intent intent = new Intent(SplashActivity.this,HomeActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }else{
            Toast.makeText(this, "Please check your internet", Toast.LENGTH_SHORT).show();
        }

    }


    private void setPreferredLanguage() {
        String languageToLoad  = MyPortFolioApp.getmInstance().getSharedPreferences().
                getString(getString((R.string.preference_param_user_language)), "en");
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
    }
}
